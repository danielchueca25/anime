package com.daniel.animes;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.util.HashMap;
import java.util.Map;

public class SignUp extends AppCompatActivity {
    private Button signUp;
    private Button logIn;
    static String URL, JSON;
    private EditText name, email, phone, password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        signUp = findViewById(R.id.signUpMain);
        logIn = findViewById(R.id.loginMain);
        name = findViewById(R.id.nameText);
        email = findViewById(R.id.passworText);
        password = findViewById(R.id.emailText);
        phone = findViewById(R.id.phoneText);

        logIn.setOnClickListener(v -> {
            Intent intent = new Intent(SignUp.this, LoginActivity.class);
            startActivity(intent);
            finish();
        });

        signUp.setOnClickListener(v -> {
            if (isEmpty(name) || isEmpty(email) || isEmpty(password) || isEmpty(phone))
                Toast.makeText(getApplicationContext(), "Tienes que rellenar todos los campos", Toast.LENGTH_SHORT).show();

            else {
                URL = "https://joanseculi.com/";
                JSON = URL + "edt69/createuser2.php";
                createUser();
            }

        });
        //comprobar datos metodo post
    }
    private boolean isEmpty(EditText text){
        if (text.getText().toString().trim().length() > 0)
            return false;
        return true;
    }
    private void createUser(){
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        StringRequest stringrequest = new StringRequest(
                Request.Method.POST,
                JSON,
                response -> {
                    Toast.makeText(this, "aaaa", Toast.LENGTH_SHORT).show();
                },
                error -> Log.d("tag","onErrorResponse: " + error.getMessage())){
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("name", name.getText().toString());
                params.put("email", email.getText().toString());
                params.put("password", password.getText().toString());
                params.put("phone", phone.getText().toString());
                return params;
            }
        };
        requestQueue.add(stringrequest);

        Toast.makeText(getApplicationContext(), "Go to login to enter", Toast.LENGTH_SHORT).show();
    }

}