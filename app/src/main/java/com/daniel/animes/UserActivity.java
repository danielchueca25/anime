package com.daniel.animes;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.util.HashMap;
import java.util.Map;

public class UserActivity extends AppCompatActivity {
    static String JSON_UPDATE, JSON_DELETE, URL, EMAIL;
    static User USER;
    Toolbar toolbarUser;
    private Button btnDelete, btnUpdate;
    private EditText nameUserText, emailUserText, passUserText, phoneUserText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user);
        //toolbar
        toolbarUser = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbarUser);

        // hook
        btnUpdate = findViewById(R.id.btnUpdate);
        btnDelete = findViewById(R.id.btnDelete);
        nameUserText = findViewById(R.id.nameUserText);
        emailUserText = findViewById(R.id.emailUserText);
        passUserText = findViewById(R.id.passUserText);
        phoneUserText = findViewById(R.id.phoneUserText);
        // ***********
        URL = getIntent().getStringExtra("url");
        USER = (User) getIntent().getSerializableExtra("user");
        EMAIL = USER.getEmail();
        JSON_DELETE = Server.IP + "edt69/deleteuser.php";
        JSON_UPDATE = Server.IP + "edt69/updateuser.php";

        // **********
        emailUserText.setText(USER.getEmail());
        btnUpdate.setOnClickListener(view -> {
            updateUser(USER);
        });
        btnDelete.setOnClickListener(view -> {
            deleteUser(USER);
        });
    }

    // -----------------------------------------------------------------------------
    // update user
    private void updateUser(User user){
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        StringRequest stringrequest = new StringRequest(
                Request.Method.POST,
                JSON_UPDATE,
                response -> {
                    Toast.makeText(this, user.getEmail(), Toast.LENGTH_SHORT).show();
                },
                error -> Log.d("tag","onErrorResponse: " + error.getMessage())){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("name", nameUserText.getText().toString() );
                params.put("email", emailUserText.getText().toString() );
                params.put("password", passUserText.getText().toString() );
                params.put("phone", phoneUserText.getText().toString() );
                return params;
            }
        };
        requestQueue.add(stringrequest);
    }


    // delete user
    private void deleteUser(User user){
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        StringRequest stringrequest = new StringRequest(
                Request.Method.POST,
                JSON_DELETE,
                response -> {
                    Intent i = new Intent(getApplicationContext(), LoginActivity.class);
                    startActivity(i);
                    finish();
                },
                error -> Log.d("tag","onErrorResponse: " + error.getMessage())){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("email", USER.getEmail() );
                return params;
            }
        };
        requestQueue.add(stringrequest);
    }

    // -----------------------------------------------------------------------------
    // toolbar

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.my_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        Intent i;
        switch (item.getItemId()) {
            case R.id.list:
                // Toast.makeText(this, "Menu", Toast.LENGTH_SHORT).show();
                i = new Intent(getApplicationContext(), AnimesMain.class);
                i.putExtra("user", USER);
                startActivity(i);
                finish();
                return true;

            case R.id.profile:
                // Toast.makeText(this, "Profile", Toast.LENGTH_SHORT).show();
                i = new Intent(getApplicationContext(), UserActivity.class);
                i.putExtra("user", USER);
                startActivity(i);
                finish();
                return true;

            case R.id.favorites:
                // Toast.makeText(this, "Fav", Toast.LENGTH_SHORT).show();
                i = new Intent(getApplicationContext(), FavoriteActivity.class);
                i.putExtra("user", USER);
                startActivity(i);
                finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }
}