package com.daniel.animes;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class MyAdapterFav extends RecyclerView.Adapter<MyAdapterFav.MyViewHolder> {
    private Context context;
    private ArrayList<Anime> animes;

    static String INSERT_URL = "https://www.joanseculi.com/edt69/insertfavorite.php";
    static String DELETE_URL = "https://www.joanseculi.com/edt69/deletefavorite.php";


    public MyAdapterFav(ArrayList<Anime> animes, Context context) {
        this.context = context;
        this.animes = animes;

    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private ImageView imageAnime;
        private TextView animeName;
        private TextView desc;
        private TextView anyo;
        private TextView platform;
        private ImageView likeOff;


        RelativeLayout relativeLayout;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            imageAnime = itemView.findViewById(R.id.imageAnime);
            animeName = itemView.findViewById(R.id.animeName);
            desc = itemView.findViewById(R.id.desc);
            anyo = itemView.findViewById(R.id.anyo);
            platform = itemView.findViewById(R.id.platform);
            likeOff = itemView.findViewById(R.id.likeOff);
        }
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.row_recycler, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {


        Picasso.get().load(Server.IP + animes.get(position).getImage())
                .fit()
                .centerCrop()
                .into(holder.imageAnime);
        holder.animeName.setText(animes.get(position).getName());
        holder.desc.setText(animes.get(position).getDescription());
        holder.anyo.setText(String.valueOf(animes.get(position).getYear()));
        holder.platform.setText(animes.get(position).getType());

        //holder.likeOff.setImageResource(R.drawable.ic_fav);


       /* if (animes.get(position).getFavorite() == "null") {
            holder.likeOff.setImageResource(R.drawable.ic_fav);
        } else {
            holder.likeOff.setImageResource(R.drawable.ic_fav_fill);
        }


        holder.likeOff.setOnClickListener(view -> {

            if (animes.get(position).getFavorite().equals("null")) {
                insertFav(email, animes.get(position), holder.likeOff);
            } else {
                deleteFav(email, animes.get(position), holder.likeOff);
            }
        });*/

    }

  /*  private void insertFav(String email, Anime anime, ImageView fav) {

        RequestQueue requestQueue = Volley.newRequestQueue(context.getApplicationContext());
        StringRequest stringrequest = new StringRequest(
                Request.Method.POST,
                INSERT_URL,
                response -> {
                    fav.setImageResource(R.drawable.ic_fav_fill);
                },
                error -> Log.d("tag", "onErrorResponse: " + error.getMessage())) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("email", email);
                params.put("anime", anime.getName());
                return params;
            }
        };
        requestQueue.add(stringrequest);
    }

    private void deleteFav(String email, Anime anime, ImageView fav) {

        RequestQueue requestQueue = Volley.newRequestQueue(context.getApplicationContext());
        StringRequest stringrequest = new StringRequest(
                Request.Method.POST,
                DELETE_URL,
                response -> {
                    fav.setImageResource(R.drawable.ic_fav);
                },
                error -> Log.d("tag", "onErrorResponse: " + error.getMessage())) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("email", email);
                params.put("anime", anime.getName());
                return params;
            }
        };
        requestQueue.add(stringrequest);
    }
*/

    @Override
    public int getItemCount() {
        return animes.size();
    }


}
