package com.daniel.animes;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;


public class LoginActivity extends AppCompatActivity {
    private Button signUp;
    private Button logIn;
    private String JSON,URL;
    private EditText email, password;
    User USER = new User();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        signUp = findViewById(R.id.signUpLog);
        logIn = findViewById(R.id.loginLog);
        email = findViewById(R.id.emailText);
        password = findViewById(R.id.passworText);

        signUp.setOnClickListener(v -> {
            Intent intent = new Intent(LoginActivity.this, SignUp.class);
            startActivity(intent);
            finish();
        });

        logIn.setOnClickListener(v -> {
            //comprovar campos validos, hacer metodo recuperar pagina web login, compruebo si es null,
            //si es null no pot si no intent
            if(isEmpty(email) || isEmpty(password)){
                Toast.makeText(this, "Todos los campos son obligatorios", Toast.LENGTH_SHORT).show();
            }else{
                JSON =Server.IP +Server.HTDOCS +Server.LOGINUSER+ "?email="+ email.getText()+ "&password="+password.getText();
                System.out.println(JSON);
                getUser();
            }
        });
    }
    private boolean isEmpty(EditText text){
        if (text.getText().toString().trim().length() >0){
            return false;
        }
        return true;
    }
    private void getUser() {
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                Request.Method.GET,
                JSON,
                null,
                response -> {
                    try {
                     USER.setId(Integer.parseInt(response.getString("id")));
                     USER.setName(response.getString("name"));
                     USER.setEmail(response.getString("email"));
                     USER.setPassword(response.getString("password"));
                     USER.setPhone(response.getString("phone"));

                     Intent intent = new Intent(getApplicationContext(), AnimesMain.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.putExtra("url",URL);
                     intent.putExtra("user",USER);
                     startActivity(intent);


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                },
                error -> Log.d("tag", "OnErrorMessageResponse" + error.getMessage())
        );
        requestQueue.add(jsonObjectRequest);

    }
  /*  private void getAnimes() {
        RequestQueue queue = Volley.newRequestQueue(getApplicationContext());
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                Request.Method.GET,
                JSON ,
                null,
                (Response.Listener<JSONObject>) response -> {
                    try {
                        JSONArray jsonArray =  response.getJSONArray("animes");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject animeObject = jsonArray.getJSONObject(i);
                            Anime anime = new Anime();
                            anime.setName(animeObject.getString("name"));
                            anime.setDescription(animeObject.getString("description"));
                            anime.setYear(animeObject.getInt("year"));
                            anime.setImage(animeObject.getString("image"));
                            anime.setType(animeObject.getString("type"));
                            animes.add(anime);
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                },
                error -> Log.d("tag","onErrorResponse: " + error.getMessage())

        );


        queue.add(jsonObjectRequest);
    }*/
}