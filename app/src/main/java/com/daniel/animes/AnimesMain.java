package com.daniel.animes;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;



public class AnimesMain extends AppCompatActivity {
    Toolbar toolbar;
    RecyclerView recyclerView;
    ArrayList<Anime> animes = new ArrayList<>();
    MyAdapter myAdapter;
    User USER = new User();
    String JSON;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_animes_main);
        toolbar = findViewById(R.id.toolbar);
        recyclerView = findViewById(R.id.recycler);
        setSupportActionBar(toolbar);
        USER = (User)getIntent().getSerializableExtra("user");
        JSON = Server.IP +Server.HTDOCS +Server.ANIMES+ "?email=" + USER.getEmail();
        System.out.println(JSON);
        getAnimes();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.my_menu,menu);
        return true;
    }

    private void getAnimes() {
        RequestQueue queue = Volley.newRequestQueue(getApplicationContext());
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                Request.Method.GET,
                JSON ,
                null,
                response -> {
                    try {
                        JSONArray jsonArray =  response.getJSONArray("animes");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject animeObject = jsonArray.getJSONObject(i);
                            Anime anime = new Anime();
                            anime.setName(animeObject.getString("name"));
                            anime.setDescription(animeObject.getString("description"));
                            anime.setYear(animeObject.getInt("year"));
                            anime.setImage(animeObject.getString("image"));
                            anime.setType(animeObject.getString("type"));
                            anime.setFavorite(animeObject.getString("favorite"));
                            animes.add(anime);
                        }
                        recyclerView.setLayoutManager(new
                                LinearLayoutManager(getApplicationContext()));
                        myAdapter = new MyAdapter(animes,getApplicationContext(),USER.getEmail());
                        recyclerView.setAdapter(myAdapter);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                },
                error -> Log.d("tag","onErrorResponse: " + error.getMessage())

        );


        queue.add(jsonObjectRequest);
    }
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        Intent i;
        switch (item.getItemId()) {
            case R.id.list:
                i = new Intent(getApplicationContext(), AnimesMain.class);
                i.putExtra("user", USER);
                startActivity(i);
                finish();
                return true;
            case R.id.profile:
                i = new Intent(getApplicationContext(), UserActivity.class);
                i.putExtra("user", USER);
                startActivity(i);
                finish();
                return true;
            case R.id.favorites:

                i = new Intent(getApplicationContext(), FavoriteActivity.class);
                i.putExtra("user", USER);
                startActivity(i);
                finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }
}