package com.daniel.animes;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder> {
    private Context context;
    private ArrayList<Anime> animes;
    private String email;
    static String INSERT_URL = "https://www.joanseculi.com/edt69/insertfavorite.php";
    static String DELETE_URL = "https://www.joanseculi.com/edt69/deletefavorite.php";


    public MyAdapter(ArrayList<Anime> animes, Context context, String email) {
        this.context = context;
        this.animes = animes;
        this.email = email;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private ImageView imageAnime;
        private TextView animeName;
        private TextView desc;
        private TextView anyo;
        private TextView platform;
        private ImageView likeOff;



        RelativeLayout relativeLayout;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            imageAnime = itemView.findViewById(R.id.imageAnime);
            animeName = itemView.findViewById(R.id.animeName);
            desc = itemView.findViewById(R.id.desc);
            anyo = itemView.findViewById(R.id.anyo);
            platform = itemView.findViewById(R.id.platform);
            likeOff = itemView.findViewById(R.id.likeOff);

            relativeLayout = itemView.findViewById(R.id.rowRecycler);
        }
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.row_recycler, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, @SuppressLint("RecyclerView") int position) {


        Picasso.get().load(Server.IP + animes.get(position).getImage())
                .fit()
                .centerCrop()
                .into(holder.imageAnime);
        holder.animeName.setText(animes.get(position).getName());
        holder.desc.setText(animes.get(position).getDescription());
        holder.anyo.setText(String.valueOf(animes.get(position).getYear()));
        holder.platform.setText(animes.get(position).getType());

        holder.likeOff.setImageResource(R.drawable.ic_fav);


        if (animes.get(position).getFavorite() == "null") {
            holder.likeOff.setImageResource(R.drawable.ic_fav);
        } else {
            holder.likeOff.setImageResource(R.drawable.ic_fav_fill);
        }

        holder.likeOff.setOnClickListener(new View.OnClickListener() {
            boolean isLiked = false;

            @Override
            public void onClick(View view) {
                Drawable.ConstantState constantState;
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
                    constantState = context.getResources().getDrawable(R.drawable.ic_fav_fill, context.getTheme()).getConstantState();
                else
                    constantState = context.getResources().getDrawable(R.drawable.ic_fav_fill).getConstantState();

                System.out.println("aaaaaaaaaaaa" + constantState);
                System.out.println("aaaaaaaaaaaa" + holder.likeOff.getDrawable().getConstantState());
                if (holder.likeOff.getDrawable().getConstantState() == constantState)
                    isLiked = true;
                else
                    isLiked = false;

                if (isLiked) {
                    deleteFav(email, animes.get(position), holder.likeOff);
                } else {

                    insertFav(email, animes.get(position), holder.likeOff);
                }
            }
        });
        holder.relativeLayout.setOnClickListener(v -> {
            Intent intent = new Intent(context, DetailsActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(intent);
        });


    }

    private void insertFav(String email, Anime anime, ImageView fav) {
        System.out.println("aaaaaaa");
        RequestQueue requestQueue = Volley.newRequestQueue(context.getApplicationContext());
        StringRequest stringrequest = new StringRequest(
                Request.Method.POST,
                INSERT_URL,
                response -> {
                    fav.setImageResource(R.drawable.ic_fav_fill);
                },
                error -> Log.d("tag", "onErrorResponse: " + error.getMessage())) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("email", email);
                params.put("anime", anime.getName());
                return params;
            }
        };
        requestQueue.add(stringrequest);
    }

    private void deleteFav(String email, Anime anime, ImageView fav) {
        // Toast.makeText(context.getApplicationContext(), "Quito de fav", Toast.LENGTH_SHORT).show();

        RequestQueue requestQueue = Volley.newRequestQueue(context.getApplicationContext());
        StringRequest stringrequest = new StringRequest(
                Request.Method.POST,
                DELETE_URL,
                response -> {
                    fav.setImageResource(R.drawable.ic_fav);
                },
                error -> Log.d("tag", "onErrorResponse: " + error.getMessage())) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("email", email);
                params.put("anime", anime.getName());
                return params;
            }
        };
        requestQueue.add(stringrequest);
    }


    @Override
    public int getItemCount() {
        return animes.size();
    }


}
